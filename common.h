#ifndef __COMMON_H__
#define __COMMON_H__

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y;
    Move(int x, int y) 
    {
        this->x = x;
        this->y = y;        
    }
    Move(const Move &ot)
    {
        *this = ot;
    }
    Move() {}
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
    bool operator == (Move &ot) const 
    {
        int X = ot.getX(), Y = ot.getY();
        if (x == X && y == Y) return true;
        return false;
    }

    bool isCorner()
    {
        if (x == 0 || x == 7)
        {
            if (y == 0 || y == 7)
            {
                return true;
            }
        }
        return false;
    }

    bool isEdge()
    {
        if (x == 0 || x == 7 || y == 0 || y == 7)
        {
            return true;
        }
        return false;
    }

    bool adjCorner()
    {
    	if (x == 0 && (y == 1 || y == 6)) return true;
    	else if (x == 7 && (y == 1 || y == 6)) return true;
    	else if (y == 7 && (x == 1 || x == 6)) return true;
    	else if (y == 0 && (x == 1 || x == 6)) return true;
    	return false;
    }

    bool diagCorner()
    {
    	if (x == 1 || x == 6)
    	{
    		if (y == 1 || y == 6)
    		{
    			return true;
    		}
    	}
    	return false;
    }
};

#endif
