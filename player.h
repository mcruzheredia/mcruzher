#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <time.h>
#include <vector>
#include <cassert>

using namespace std;

class Player {

private:
	Board board;
    Side side;
    Side other_side;


public:
    Player(Side s);
    ~Player();

  Move *doMove(Move *opponentsMove, int msLeft);

  void setBoard(char data[]);

  int minimax(Board *b, Side s, Move * move, int depth = 2);

   	
  vector<Move *> getOpMoves(Board *b, Side s, Move *move);

	int Max(vector<int> s);

	int Min(vector<int> s);

  int getScore(Board *b, Side s, Move * move);

  int MaxPos(vector<int> s);

  int abpruning(Board *b, Side s, Move *m, int alpha, int beta, int depth = 2);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
