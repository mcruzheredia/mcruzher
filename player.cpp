#include "player.h"


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side s) 
{
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    board = Board();
    side = s;
    other_side = (side == BLACK) ? WHITE : BLACK;

}

/*
 * Destructor for the player.
 */
Player::~Player() 
{

}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
	// get vector of possible moves 
	vector<Move *> moves = getOpMoves(&board, other_side, opponentsMove);
	Move * ret;

	if (moves.empty())
	{
		ret = NULL;
	}

	// if running low on time switch to Heuristic
	else if (msLeft < 6000 && msLeft != -1 && !testingMinimax)
	{
		vector<int> score;

		// for each possible move
		for (int i = 0; i < moves.size(); i++)
		{
			// allot heuristic score 
			score.push_back(getScore(&board, side, moves[i]));
		}

		int count = score[0];
		int pos = 0;

		// find the best move 
		for (int i = 1; i < moves.size(); i++)
		{
			if (score[i] > count)
			{
				count = score[i];
				pos = i;
			}
		}

		ret = moves[pos];
	}
	else
	{
		int depth;
		vector<int> bestscores;

		// run minimax at different depth levels for different amounts of time left
		if (msLeft > 240000 || msLeft == -1) depth = 6;
		else if (msLeft > 120000) depth = 5;
		else if (msLeft > 60000) depth = 4;
		else if (msLeft > 20000) depth = 3;
		else depth = 2;
		if(testingMinimax) depth = 2;

		for (int i = 0; i < moves.size(); i++)
		{
			bestscores.push_back(abpruning(&board, side, moves[i], -100, 100, depth));
		}
		
		ret = moves[MaxPos(bestscores)];
	}

	// update board
	board.doMove(ret, side);
	return ret;
}

/*
* 	Sets the board to a desired configuration.
*	
*	For testing minimax.
*/
void Player::setBoard(char data[])
{
	board.setBoard(data);
}


/*
* 	Returns a list of moves that could follow a move by side s.
*/
vector<Move *> Player::getOpMoves(Board *b, Side s, Move *move)
{
	// update board after input move by side s
	b->doMove(move, s);
	// define op side
	Side ot_side = (s == side) ? other_side : side;
	// find moves op can do
	vector<Move *> move2;
	for (int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			Move *m = new Move(i, j);
			if (b->checkMove(m, ot_side))
			{
				move2.push_back(m);
			}	
		}
	}
	if (move2.empty()) move2.push_back(NULL);
	return move2;
}

/*
*	Maximum value of an int vector.
*	Used to find the best score.
*/
int Player::Max(vector<int> s)
{
	if (s.empty()) return 65;
	int counter = s[0];
	for (int i = 1; i < s.size(); ++i)
	{
		// find max score 
		if (s[i] > counter)
		{
			counter = s[i];
		}
	}
	return counter;
}

/*
*	Minimum value of an int vector.
*	Used to find the best score.
*/
int Player::Min(vector<int> s)
{
	if (s.empty()) return -65;
	int counter = s[0];
	for (int i = 1; i < s.size(); ++i)
	{
		// find min score 
		if (s[i] < counter)
		{
			counter = s[i];
		}
	}
	return counter;
}

int Player::MaxPos(vector<int> s)
{
	int counter = s[0];
	int pos = 0;
	for (int i = 1; i < s.size(); ++i)
	{
		// find max score 
		if (s[i] > counter)
		{
			counter = s[i];
			pos = i;
		}
	}
	return pos;
}

int Player::minimax(Board *b, Side s, Move * move, int depth)
{
	vector<int> score;
	Side ot_side = (s == side) ? other_side : side;

	Board *temp = b->copy();
	vector<Move *> op_moves = getOpMoves(temp, s, move); 


	if (depth == 1)
	{
		int scr = getScore(b, s, move);
		return scr;
	}
	for (int i = 0; i < op_moves.size(); i++)
	{
		int x = minimax(temp, ot_side, op_moves[i], depth - 1);
		score.push_back(x);
	}
	if (s == other_side)
	{
		return Max(score);
	}
	else
	{
		return Min(score);
	}
}


int Player::abpruning(Board *b, Side s, Move *move, int alpha, int beta, int depth)
{
	if (depth == 1 || move == NULL)
	{
		return getScore(b, s, move);
	}

	Side ot_side = (s == side) ? other_side : side;

	Board *temp = b->copy();
	vector<Move *> op_moves = getOpMoves(temp, s, move); 

	if (s == other_side)
	{
		int v = -100;
		for (int i = 0; i < op_moves.size(); i++)
		{
			v = max(v, abpruning(temp, ot_side, op_moves[i], alpha, beta, depth - 1));
			alpha = max(v, alpha);
			if (beta < alpha) break;
		}
		return v;
	}
	else
	{
		int v = 100;
		for (int i = 0; i < op_moves.size(); i++)
		{
			v = min(v, abpruning(temp, ot_side, op_moves[i], alpha, beta, depth - 1));
			beta = min(beta, v);
			if (beta < alpha) break;
		}		
		return v;
	}
}


int Player::getScore(Board *b, Side s, Move * m)
{
	Board *temp = b->copy();
	temp->doMove(m, s);
	int score = temp->count(side) - temp->count(other_side);

	if (temp->count(other_side) == 0)
	{
		return 100;
	}

	if (m == NULL)
	{
		if (s == side) score = -100;
		else score = 100;
	}
	else if (m->isCorner())
	{
		if (s == side) score += 35;
		else score += -35;
	}
	else if (m->adjCorner())
	{
		if (s == side) score += -25; 
		else score += 25;
	}
	else if (m->isEdge())
	{
		if (s == side) score += 25;	
		else score += -25;
	}
	else if (m->diagCorner())
	{
		if (s == side) score += -35;
		else score += 35;
	}
	
	return score;
}



























